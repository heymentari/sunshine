package sunshine;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class java

{
	// ---( internal utility methods )---

	final static java _instance = new java();

	static java _newInstance() { return new java(); }

	static java _cast(Object o) { return (java)o; }

	// ---( server methods )---




	public static final void readPipeline (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(readPipeline)>> ---
		// @sigtype java 3.5
		// [i] record:0:required document
		// [o] record:0:required document
		//start Here
				IDataCursor pipeCrs = pipeline.getCursor();
				IData document = (IData) ValuesEmulator.get(pipeline, "document");
				IData newIData = IDataFactory.create();
				
				
				while (pipeCrs.next()){
					if (pipeCrs.getValue() instanceof IData){
						IData Parents = (IData) ValuesEmulator.get(pipeline, pipeCrs.getKey());
						iterateData(Parents, newIData);
					} else if (pipeCrs.getValue() instanceof IData[]) {
						IData[] parentsArr = (IData[]) pipeCrs.getValue();
						int pipeIDataLength = parentsArr.length;
						for (int i = 0; i < pipeIDataLength ; i++){
							iterateData(parentsArr[i], newIData);
						}
					}
				}
				ValuesEmulator.put(pipeline,"document",newIData);
				pipeCrs.destroy();
			
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static int idxResult = 0;
	private static void iterateData(IData process, IData pipelineResult) {
		IDataCursor _c = process.getCursor();
		IDataCursor pipeCrs2 = pipelineResult.getCursor();
		while (_c.next()) {
			if (_c.getValue() instanceof IData) {
				IData child = (IData) _c.getValue();
				iterateData(child, pipelineResult);
			}
			else if (_c.getValue() instanceof IData[]) {
				IData[] child = (IData[]) _c.getValue();
				for (int i = 0; i < child.length; i++) {
					iterateData(child[i], pipelineResult);
				}
			}
			else {
				//ValuesEmulator.put(pipeCrs2, _c.getValue().toString(), "");
				idxResult ++;
				ValuesEmulator.put(pipeCrs2, "Result" + idxResult, _c.getValue());
	
			}
		}
		_c.destroy();
	}
	// --- <<IS-END-SHARED>> ---
}

